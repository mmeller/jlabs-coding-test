package com.devskiller.service;

import com.devskiller.dao.ItemRepository;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.stereotype.Service;

@Service
public class ItemService {

    private final ItemRepository itemRepository;

    public ItemService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public List<String> getTitlesWithAverageRatingLowerThan(Double rating) {
        return this.itemRepository
            .findItemsWithAverageRatingLowerThan(rating)
            .stream()
            .flatMap(i -> Stream.of(i.getTitle()))
            .collect(Collectors.toList())
        ;
    }

}
