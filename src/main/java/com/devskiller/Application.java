package com.devskiller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages={"com.devskiller"})
@EnableJpaRepositories(basePackages="com.devskiller.dao")
public class Application {

    public static void main(String[] args) {

        SpringApplication.run(Application.class, args);
    }
}
