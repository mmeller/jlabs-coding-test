package com.devskiller.dao;

import com.devskiller.model.Item;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {

    //TODO
    default List<Item> findItemsWithAverageRatingLowerThan(Double rating) {
        return this.findAll((root, query, cb) -> {
            int result = Stream.of(root.get(Item.reviews)).reduce(0, (subtotal, review) -> subtotal + review.rating);
            return cb.lessThan(result / root.get(Item.reviews).size(), rating);
        });
    }
}
